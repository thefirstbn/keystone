var React = require('react');
var CreateForm = require('../components/CreateForm');
var UploadForm = require('../components/UploadForm');

var View = React.createClass({

    displayName: 'ListView',

    getInitialState: function() {
        return {
            createIsVisible: Keystone.showCreateForm,
            uploadIsVisible: Keystone.showUploadForm,
            animateCreateForm: false,
            animateUploadForm: false
        };
    },

    toggleCreate: function(visible) {
        this.setState({
            createIsVisible: visible,
            animateCreateForm: true
        });
    },

    toggleUpload: function(visible) {
        this.setState({
            uploadIsVisible: visible,
            animateUploadForm: true
        });
    },

    renderCreateButton: function() {
        if (Keystone.list.autocreate) {
            return (
                <a href={'?new' + Keystone.csrf.query} className="btn btn-default btn-create btn-create-item">
                    <span className="ion-plus-round mr-5" />
                    Create {Keystone.list.singular}
                </a>
            );
        }
        return (
            <button type="button" className="btn btn-default btn-create btn-create-item" onClick={this.toggleCreate.bind(this, true)}>
                <span className="ion-plus-round mr-5" />
                Create {Keystone.list.singular}
            </button>
        );
    },

    renderCreateForm: function() {
        if (!this.state.createIsVisible) return null;
        return <CreateForm list={Keystone.list} animate={this.state.animateCreateForm} onCancel={this.toggleCreate.bind(this, false)} values={Keystone.createFormData} err={Keystone.createFormErrors} />;
    },

    renderUploadButton: function() {
        if (Keystone.list.singular === 'Menu') {
            return (
                <button type="button" className="btn btn-default btn-create btn-create-item" onClick={this.toggleUpload.bind(this, true)}>
                    <span className="ion-plus-round mr-5" />
                    Upload CSV for {Keystone.list.singular}
                </button>
            );
        }
    },

    renderUploadForm: function() {
        if (!this.state.uploadIsVisible) return null;
        return <UploadForm list={Keystone.list} animate={this.state.animateCreateForm} onCancel={this.toggleUpload.bind(this, false)} />;
    },

    render: function() {
        if (Keystone.list.nocreate) return null;
        return (
            <div className="create-item">
                <div className="toolbar">
                    {this.renderCreateButton()}
                    {this.renderUploadButton()}
                </div>
                {this.renderCreateForm()}
                {this.renderUploadForm()}
                <hr />
            </div>
        );
    }

});

React.render(<View />, document.getElementById('list-view'));
