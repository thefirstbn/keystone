var _ = require('underscore'),
    React = require('react');

var Form = React.createClass({

    displayName: 'UploadForm',

    render: function() {

        var form = null,
            list = this.props.list,
            formAction = '/uploadcsv_menu';
        var modalClass = 'modal modal-md' + (this.props.animate ? ' animate' : '');

        form = (
            <div>
            <h4>Your CSV file content should be the below format:</h4>
            <pre>
            date       stall    name          hall     category<br />
            6/25/2015  INDIAN   EGG BHURJI    Hall_1   Breakfast<br />
            6/26/2015  CHINESE  CHICKEN RICE  Hall_2   Dinner<br />
            ......<br />
            </pre>
            <input type="file" id="csvFile" accept=".csv" name="csvFile"/>
            </div>
        );

        return (
            <div>
                <div className={modalClass}>
                    <div className="modal-dialog">
                        <form className="modal-content" encType="multipart/form-data" method="post" action={formAction}>
                            <input type="hidden" name="action" value="create" />
                            <input type="hidden" name={Keystone.csrf.key} value={Keystone.csrf.value} />
                            <div className="modal-header">
                                <button type="button" className="modal-close" onClick={this.props.onCancel}></button>
                                <div className="modal-title">Upload CSV for {list.singular}</div>
                            </div>
                            <div className="modal-body">
                                {form}
                            </div>
                            <div className="modal-footer">
                                <button type="submit" name="submit" className="btn btn-create">Upload</button>
                                <button type="button" className="btn btn-link btn-cancel" onClick={this.props.onCancel}>cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="modal-backdrop"></div>
            </div>
        );
    }

});

module.exports = Form;